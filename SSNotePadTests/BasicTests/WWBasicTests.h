//  WWBasicTests.h
//  WildWalks
//
//  Created by Sergiy Shevchuk on 7/11/16.
//  Copyright © 2016 Sergiy Shevchuk. All rights reserved.

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <Expecta/Expecta.h>
#import <OHHTTPStubs/OHHTTPStubs.h>

@interface WWBasicTests : XCTestCase

@end
