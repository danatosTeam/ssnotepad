//  SSModel.m
//  SSNotePad
//
//  Created by Sergiy Shevchuk on 7/31/16.
//  Copyright © 2016 Sergiy Shevchuk. All rights reserved.

#import "SSModel.h"

@implementation SSModel

#pragma mark - Initialisation and Dealoccation

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    
    return self;
}

#pragma mark - Protocols Methods

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {

}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

@end
