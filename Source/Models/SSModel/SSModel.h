//  Created by Sergiy Shevchuk on 7/31/16.
//  Copyright © 2016 Sergiy Shevchuk. All rights reserved.

#import <Foundation/Foundation.h>
#import "SSBlockTypeDef.h"
#import "SSWeakStrongMacros.h"
#import "SSEntityKeys.h"

@interface SSModel : NSObject <NSCoding, NSCopying>

@end
