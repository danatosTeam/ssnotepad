//  SSTask.h
//  SSNotePad
//
//  Created by Sergiy Shevchuk on 8/8/16.
//  Copyright © 2016 Sergiy Shevchuk. All rights reserved.

#import "SSModel.h"

@class SSSubTask;

@interface SSTask : SSModel
@property (nonatomic, readonly) NSString    *title;
@property (nonatomic, readonly) NSDate      *lastUpdate;

@property (nonatomic, readonly) NSArray     *subTasks;

@property (nonatomic, assign, getter=isCompleted)   BOOL    completed;
@property (nonatomic, assign, getter=isAllSubtasksCompleted)    BOOL   allSubtasksCompleted;

+ (instancetype)initTaskWitlTitle:(NSString *)title;

- (void)addSubTask:(SSSubTask *)subTask;
- (void)removeSubTask:(SSSubTask *)subTask;
- (void)removeAllSubTasks;
- (void)markAllSubTasksDone;

- (NSUInteger)subTasksCount;

@end
