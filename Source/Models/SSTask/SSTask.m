//  SSTask.m
//  SSNotePad
//
//  Created by Sergiy Shevchuk on 8/8/16.
//  Copyright © 2016 Sergiy Shevchuk. All rights reserved.

#import "SSTask.h"

@interface SSTask ()
@property (nonatomic, strong) NSMutableString   *mutableTitle;
@property (nonatomic, strong) NSMutableArray    *mutableSubTasks;
@property (nonatomic, strong) NSDate            *lastModification;

@end

@implementation SSTask

@dynamic title;
@dynamic lastUpdate;
@dynamic subTasks;

#pragma mark - Class Methods

+ (instancetype)initTaskWitlTitle:(NSString *)title {
    SSTask *task = [SSTask new];
    task.mutableTitle = [title mutableCopy];
    task.mutableSubTasks = [NSMutableArray array];
    
    return task;
}

#pragma mark - Initialization and Dealoccation

- (instancetype)init {
    self = [super init];
    if (self) {
        self.lastModification = [NSDate date];
    }
    
    return self;
}

#pragma mark - Accessors

- (NSString *)title {
    return [self.mutableTitle copy];
}

- (NSArray *)subTasks {
    return [self.mutableSubTasks copy];
}

- (NSDate *)lastUpdate {
    return [self.lastModification copy];
}

#pragma mark - Public

- (void)addSubTask:(SSSubTask *)subTask {
    
}

- (void)removeSubTask:(SSSubTask *)subTask {
    
}

- (void)removeAllSubTasks {
    
}

- (void)markAllSubTasksDone {
    
}

- (NSUInteger)subTasksCount {
    return self.mutableSubTasks.count;
}

#pragma mark - Private

- (void)updateLatsModificationDate {
    self.lastModification = [NSDate date];
}

@end
