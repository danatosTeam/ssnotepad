//  Created by Sergiy Shevchuk on 7/31/16.
//  Copyright © 2016 Sergiy Shevchuk. All rights reserved.

#define SSWeakify(variable) \
__weak __typeof(variable) __SSWeakified_##variable = variable

// you should only call this method after you called weakify for that same variable
#define SSStrongify(variable) \
__strong __typeof(variable) variable = __SSWeakified_##variable \
