//  Created by Sergiy Shevchuk on 7/31/16.
//  Copyright © 2016 Sergiy Shevchuk. All rights reserved.

#ifndef SSBlockTypeDef_h
#define SSBlockTypeDef_h

typedef void (^SSVoidBlock)(void);
typedef void (^SSResultBlock)(id result);
typedef void (^SSArrayBlock)(NSArray *array);
typedef void (^SSDictionaryBlock)(NSDictionary *dictionary);
typedef void (^SSErrorBlock)(NSError *error);

#endif /* SSBlockTypeDef_h */
