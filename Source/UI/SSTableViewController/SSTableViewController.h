//  SSTableViewController.h
//  SSNotePad
//
//  Created by Sergiy Shevchuk on 8/3/16.
//  Copyright © 2016 Sergiy Shevchuk. All rights reserved.

#import "SSViewController.h"

@class SSTableDataDelegate;

@interface SSTableViewController : SSViewController
@property (nonatomic, strong) IBOutlet UITableView *notesTable;
@property (nonatomic, readonly) SSTableDataDelegate *tableDelegate;

- (IBAction)removeButton:(id)sender;
- (IBAction)editButton:(id)sender;
- (IBAction)addButton:(id)sender;

@end
