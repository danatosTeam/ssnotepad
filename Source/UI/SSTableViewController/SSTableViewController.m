//  SSTableViewController.m
//  SSNotePad
//
//  Created by Sergiy Shevchuk on 8/3/16.
//  Copyright © 2016 Sergiy Shevchuk. All rights reserved.

#import "SSTableViewController.h"

@implementation SSTableViewController

#pragma mark - ViewController Life Cycle

- (void)viewDidLoad {
    
}

#pragma mark - Initialization and Dealoccation

#pragma mark - Accessors

#pragma mark - Action buttons

- (IBAction)removeButton:(id)sender {
}

- (IBAction)editButton:(id)sender {
}

- (IBAction)addButton:(id)sender {
}
@end
