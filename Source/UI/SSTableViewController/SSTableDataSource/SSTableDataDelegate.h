//  SSTableDataDelegate.h
//  SSNotePad
//
//  Created by Sergiy Shevchuk on 8/3/16.
//  Copyright © 2016 Sergiy Shevchuk. All rights reserved.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SSTableDataDelegate : NSObject <UITableViewDataSource, UITableViewDelegate>

//- (void)updateTableWithModels:(NSArray *)modelsArray;

@end
