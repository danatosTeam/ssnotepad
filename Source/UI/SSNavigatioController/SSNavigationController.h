//
//  SSNavigationController.h
//  SSNotePad
//
//  Created by Sergiy Shevchuk on 8/3/16.
//  Copyright © 2016 Sergiy Shevchuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSNavigationController : UINavigationController

@end
